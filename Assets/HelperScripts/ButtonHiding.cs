﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonHiding : MonoBehaviour {
    [SerializeField]
    public GameObject[] ButtonsToHide;
    [SerializeField]
    public GameObject[] ButtonsToShow;
    [SerializeField]
    bool runonstart = false;
    // Use this for initialization
    void Start () {
        if (runonstart)
        {
            hide();
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void hide()
    {
        foreach (var item in ButtonsToHide)
        {
            item.SetActive(false);
        }
        foreach (var item in ButtonsToShow)
        {
            item.SetActive(true);
        }
        
    }
}
