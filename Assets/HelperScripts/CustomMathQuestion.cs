﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomMathQuestion : MonoBehaviour {

    [SerializeField]
    Text AdditionProblem;
    [SerializeField]
    Text SubstractionProblem;
    [SerializeField]
    Text MultiplicationProblem;
    [SerializeField]
    CheckValue Addition;
    [SerializeField]
    CheckValue Substraction;
    [SerializeField]
    CheckValue Multiplication;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void generateProblems()
    {
        int n1 = Random.Range(-1000, 1000);
        int n2 = Random.Range(-1000, 1000);
        AdditionProblem.text = n1 + " + " + n2 + " =";
        Addition.value = n1 + n2;
        n1 = Random.Range(-1000, 1000);
        n2 = Random.Range(-1000, 1000);
        SubstractionProblem.text = n1 + " - " + n2 + " =";
        Substraction.value = n1 - n2;
        n1 = Random.Range(-30, 30);
        n2 = Random.Range(-30, 30);
        MultiplicationProblem.text = n1 + " * " + n2 + " =";
        Multiplication.value = n1 * n2;
    }
    
}
