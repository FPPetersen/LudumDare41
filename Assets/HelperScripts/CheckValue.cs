﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class CheckValue : MonoBehaviour {
    public float value;
    [SerializeField]
    Text inputField;
    [SerializeField]
    string scene;
    [SerializeField]
    AudioSource clickSound;
    [SerializeField]
    AudioClip[] clickSounds;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void checkValue()
    {
        if(inputField.text != "" )
        {
            if(value == float.Parse(inputField.text))
            {
                clickSound.clip = clickSounds[0];
                clickSound.Play();
                SceneManager.LoadScene(scene);
            }
            else
            {
                clickSound.clip = clickSounds[1];
                clickSound.Play();
            }

        }
        else
        {
            clickSound.clip = clickSounds[1];
            clickSound.Play();
        }

    }
}
