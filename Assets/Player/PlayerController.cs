﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    [SerializeField]
    Rigidbody2D rb2d;
    [SerializeField]
    public float maxSpeed;
    [SerializeField]
    public float defaultAccel;
    [SerializeField]
    public float jumpForce;
    [SerializeField]
    public BoxCollider2D collision;
    [SerializeField]
    public float currentValue;
    [SerializeField]
    public string operation;
    [SerializeField]
    SpriteRenderer[] digits;
    [SerializeField]
    Sprite[] digitsSprite;
    [SerializeField]
    SpriteRenderer[] eyes;
    [SerializeField]
    Sprite eye;
    [SerializeField]
    Animator anim;
    [SerializeField]
    AudioSource audio;
    [SerializeField]
    AudioClip[] sounds;
    [SerializeField]
    AudioSource collect;
    [SerializeField]
    GameObject pauseScreen;
    public bool enableControls = true;
    private bool flipped = false;
    private bool jump = false;
    private bool grounded = false;
    private Vector3 lastPos;
    public Transform groundCheck;
    private float stepTime = 0;
    private bool pause = false;


    // Use this for initialization
    void Start () {
        enableControls = true;
        pauseScreen.SetActive(false);
        updateVisuals();
    }
	
	// Update is called once per frame
	void Update () {
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
        if (grounded && Input.GetButtonDown("Jump"))
        {
            jump = true;
        }
        if(grounded)
        {
            lastPos = transform.position;
        }
        if(grounded && !audio.isPlaying && rb2d.velocity.x != 0 && stepTime > .5)
        {
            audio.clip = sounds[0];
            audio.pitch = Random.Range(.8f, 1.2f);
            audio.Play();
            stepTime = 0;
        }
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(pause)
            {
                pause = false;
                enableControls = true;
                pauseScreen.SetActive(false);
                anim.SetBool("levelOver", false);
            }
            else
            {
                pause = true;
                enableControls = false;
                pauseScreen.SetActive(true);
            }
        }
        stepTime += Time.deltaTime;
    }

     void FixedUpdate()
    {
        if(enableControls)
        {
            float accel = Input.GetAxis("Horizontal");
            if (accel < 0 && !flipped)
            {
                flipped = true;
                updateVisuals();
            }
            else if (accel > 0 && flipped)
            {
                flipped = false;
                updateVisuals();

            }
            if (accel * rb2d.velocity.x < maxSpeed)
            {
                rb2d.AddForce(Vector2.right * accel * defaultAccel);
            }
            if (accel == 0 && grounded)
            {
                rb2d.velocity = new Vector2(0, rb2d.velocity.y);
            }
            if (Mathf.Abs(rb2d.velocity.x) > maxSpeed)
            {
                rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);
            }
            if (jump)
            {
                rb2d.AddForce(new Vector2(0f, jumpForce));
                jump = false;
                audio.Stop();
                audio.volume = .2f;
                audio.clip = sounds[1];
                audio.Play();
            }
            anim.SetFloat("speed", rb2d.velocity.x);
        }else
        {
            anim.SetBool("levelOver", true);
        }
        
       
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Number")
        {

            Number number = collision.gameObject.GetComponent<Number>();
            if(!number.pickedUp)
            {
                number.pickedUp = true;
                switch (operation)
                {
                    case "plus":
                        {
                            currentValue += number.value;
                            break;
                        }
                    case "minus":
                        {
                            currentValue -= number.value;
                            break;
                        }
                    case "multiply":
                        {
                            currentValue *= number.value;
                            break;
                        }
                    case "devide":
                        {
                            currentValue /= number.value;
                            break;
                        }
                    default:
                        break;
                }
                collect.Play();
                updateVisuals();
                Destroy(collision.gameObject);
            }
           
        }else if(collision.tag == "DeathZone")
        {
            transform.position = lastPos;
        }
    }
    private void updateVisuals()
    {
        int temp = (int)currentValue;
        List<int> listOfDigits = new List<int>();
        while (temp > 0)
        {
            listOfDigits.Add(temp % 10);
            temp = temp / 10;
        }
        switch(listOfDigits.Count)
        {
            case 1:
                {
                    digits[0].sprite = null; 
                    digits[1].sprite = digitsSprite[listOfDigits[0]];
                    digits[2].sprite = null;
                    transform.Find("EyeRight").transform.localPosition = new Vector3(-7.6f, 11f, 0);
                    transform.Find("EyeLeft").transform.localPosition = new Vector3(7.3f, 11f, 0);
                    if (flipped)
                    {
                        eyes[1].sprite = eye;
                        eyes[0].sprite = null;
                        transform.Find("10").transform.Find("GameObject").transform.localScale = new Vector3(-1f, transform.Find("10").transform.Find("GameObject").transform.localScale.y, transform.Find("10").transform.Find("GameObject").transform.localScale.z);
                        transform.Find("10").transform.Find("GameObject").transform.localPosition = new Vector3(7.73f, 7.3f, 0);
                    }
                    else
                    {
                        eyes[0].sprite = eye;
                        eyes[1].sprite = null;
                        transform.Find("10").transform.Find("GameObject").transform.localScale = new Vector3(1f, transform.Find("10").transform.Find("GameObject").transform.localScale.y, transform.Find("10").transform.Find("GameObject").transform.localScale.z);
                        transform.Find("10").transform.Find("GameObject").transform.localPosition = new Vector3(-5.73f, 7.3f, 0);
                    }
                    break;
                }
            case 2:
                {
                    digits[0].sprite = digitsSprite[listOfDigits[0]];
                    digits[1].sprite = digitsSprite[listOfDigits[1]];
                    digits[2].sprite = null;
                    transform.Find("EyeRight").transform.localPosition = new Vector3(-.9f, 11f, 0);
                    transform.Find("EyeLeft").transform.localPosition = new Vector3(7.3f, 11f, 0);
                    if (flipped)
                    {
                        eyes[1].sprite = eye;
                        eyes[0].sprite = null;
                        transform.Find("10").transform.Find("GameObject").transform.localScale = new Vector3(transform.Find("10").transform.Find("GameObject").transform.localScale.x * -1f, transform.Find("10").transform.Find("GameObject").transform.localScale.y, transform.Find("10").transform.Find("GameObject").transform.localScale.z);
                        transform.Find("10").transform.Find("GameObject").transform.localPosition = new Vector3(11.36f, 7.3f, 0);
                    }
                    else
                    {
                        eyes[0].sprite = eye;
                        eyes[1].sprite = null;
                        transform.Find("10").transform.Find("GameObject").transform.localScale = new Vector3(1f, transform.Find("10").transform.Find("GameObject").transform.localScale.y, transform.Find("10").transform.Find("GameObject").transform.localScale.z);
                        transform.Find("10").transform.Find("GameObject").transform.localPosition = new Vector3(-3.6f, 7.3f, 0);
                    }
                    break;
                }
            case 3:
                {
                    digits[0].sprite = digitsSprite[listOfDigits[0]];
                    digits[1].sprite = digitsSprite[listOfDigits[1]];
                    digits[2].sprite = digitsSprite[listOfDigits[2]];
                    transform.Find("EyeRight").transform.localPosition = new Vector3(-.9f, 11f, 0);
                    transform.Find("EyeLeft").transform.localPosition = new Vector3(.9f, 11f, 0);
                    if (flipped)
                    {
                        eyes[1].sprite = eye;
                        eyes[0].sprite = null;
                        transform.Find("10").transform.Find("GameObject").transform.localScale = new Vector3( -1f, transform.Find("10").transform.Find("GameObject").transform.localScale.y, transform.Find("10").transform.Find("GameObject").transform.localScale.z);
                        transform.Find("10").transform.Find("GameObject").transform.localPosition = new Vector3(7.73f, 7.3f, 0);
                    }
                    else
                    {
                        eyes[0].sprite = eye;
                        eyes[1].sprite = null;
                        transform.Find("10").transform.Find("GameObject").transform.localScale = new Vector3(1f, transform.Find("10").transform.Find("GameObject").transform.localScale.y, transform.Find("10").transform.Find("GameObject").transform.localScale.z);
                        transform.Find("10").transform.Find("GameObject").transform.localPosition = new Vector3(-5.73f, 7.3f, 0);
                    }
                    break;
                }
            default:
                { break; }
        }
       
            

        
    }
   
}
