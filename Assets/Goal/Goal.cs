﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Goal : MonoBehaviour {
    [SerializeField]
    public float target;
    [SerializeField]
    PlayerController player;
    [SerializeField]
    BoxCollider2D collision;
    [SerializeField]
    GameObject endscreen;
    [SerializeField]
    Sprite[] endscreens;
	// Use this for initialization
	void Start () {
        endscreen.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
     
        if (collision.tag == "Player")
        {
            float playerValue = player.currentValue;
            float percentage = playerValue / target;
            Image temp = endscreen.transform.Find("Canvas").transform.Find("Image").GetComponent<Image>();
            if (percentage == 1)
            {
                temp.sprite = endscreens[2];
            }
            else if (percentage > .8 && percentage < 1.2)
            {
                temp.sprite = endscreens[1];
            }
            else if (percentage <= .8  && percentage > .6|| percentage >= 1.2 && percentage < 1.4)
            {
                temp.sprite = endscreens[0];
            }
            else if (percentage <= .6 || percentage >= 1.4)
            {
                temp.sprite = endscreens[0];
            }
            endscreen.SetActive(true);
            player.enableControls = false;
        }
    }
}
