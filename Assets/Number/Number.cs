﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Number : MonoBehaviour {

    [SerializeField]
    public float value = 0;
    [SerializeField]
    Sprite[] digitsSprite;
    [SerializeField]
    SpriteRenderer[] digits;
    public bool pickedUp = false;

    // Use this for initialization
    void Start()
    {
        int temp = (int)value;
        List<int> listOfDigits = new List<int>();
        while (temp > 0)
        {
            listOfDigits.Add(temp % 10);
            temp = temp / 10;
        }
        switch (listOfDigits.Count)
        {
            case 1:
                {
                    digits[0].sprite = null;
                    digits[1].sprite = digitsSprite[listOfDigits[0]];


                    break;
                }
            case 2:
                {
                    digits[0].sprite = digitsSprite[listOfDigits[0]];
                    digits[1].sprite = digitsSprite[listOfDigits[1]];

                    break;
                }
            default:
                { break; }
        }
    }
	// Update is called once per frame
	void Update () {
		
	}
}
